---
codename: 'oneplus3'
name: 'Oneplus 3'
comment: 'community device'
icon: 'phone'
maturity: .8
---

You can install Ubuntu Touch on the Oneplus 3.